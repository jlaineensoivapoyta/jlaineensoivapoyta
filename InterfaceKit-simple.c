
// Copyright 2015 Jari Suominen
// This work is licensed under the Creative Commons Attribution 2.5 Canada License. 
// view a copy of this license, visit http://creativecommons.org/licenses/by/2.5/ca/

#include <stdio.h>
#include <phidget21.h>
#include <lo/lo.h>
#include <math.h>

lo_address transmit_socket, transmit_socket_2;

//callback that will run if the sensor value changes by more than the OnSensorChange trigger.
//Index - Index of the sensor that generated the event, Value - the sensor read value
int CCONV SensorChangeHandler(CPhidgetInterfaceKitHandle IFK, void *usrptr, int index, int value)
{
	int serialNo;
  double output;
	CPhidget_getSerialNumber((CPhidgetHandle)IFK, &serialNo);
	//printf("%i Sensor: %d > Value: %d\n", serialNo, index, value);
	char buffer[100];
	

  if ( serialNo == 316966 ) {
    sprintf(buffer, "/midi/cc%i/%i", serialNo == 316966, index + 1);
    output = 1.05*(value/1000.0) - 0.025;
    if (index > 3) { 
      if (output > 1) output = 1;
      else if (output < 0 ) output = 0;
      /* sliders */
      lo_send(transmit_socket, buffer, "f", 1 - output); 
    } else {
      if (output > 1) output = 1;
      else if (output < 0 ) output = 0;
      /* pots */
      lo_send(transmit_socket, buffer, "f", output); 
    }
  } else {
    if (index < 6 ) {
      /* joysticks */
      output = (value - 500.0) / 500.0;
      if (output < 0) {
        output = output * -1.0;
        output = 1.05*output - 0.025;
        sprintf(buffer, "/midi/cc%i/%i", serialNo == 316966, index*2 + 1 ); 
      }
      else {
        output = 1.05*output - 0.025;
        sprintf(buffer, "/midi/cc%i/%i", serialNo == 316966, index*2 + 2 ); 
      }
      lo_send(transmit_socket_2, buffer, "f", output); 
    } else if (index == 7) {
      sprintf(buffer, "/midi/cc%i/15", serialNo == 316966); 
      
      output = value/50.0 + 0.5;
      if (output > 1) output = 1;
      //output = 1 - output;
//printf("%f etaisyys \n", output);
      lo_send(transmit_socket, buffer, "f",output); 
    } 
  }
  
	return 0;
}

int CCONV AttachHandler(CPhidgetHandle IFK, void *userptr)
{
	int serialNo;
	const char *name;
	CPhidget_getDeviceName(IFK, &name);
	CPhidget_getSerialNumber(IFK, &serialNo);
	printf("%s %10d attached! HEIL\n", name, serialNo);
  display_properties(IFK);
	return 0;
}

int CCONV DetachHandler(CPhidgetHandle IFK, void *userptr)
{
	int serialNo;
	const char *name;
	CPhidget_getDeviceName (IFK, &name);
	CPhidget_getSerialNumber(IFK, &serialNo);
	printf("%s %10d detached!\n", name, serialNo);
	return 0;
}

int CCONV ErrorHandler(CPhidgetHandle IFK, void *userptr, int ErrorCode, const char *unknown)
{
	printf("Error handled. %d - %s", ErrorCode, unknown);
	return 0;
}

//callback that will run if an input changes.
//Index - Index of the input that generated the event, State - boolean (0 or 1) representing the input state (on or off)
int CCONV InputChangeHandler(CPhidgetInterfaceKitHandle IFK, void *usrptr, int index, int State)
{
	
	int serialNo;
	CPhidget_getSerialNumber((CPhidgetHandle)IFK, &serialNo);
	printf("%i Digital Input: %d > State: %d\n", serialNo, index, State);
  
	char buffer[100];
	
  if (serialNo == 316966) {
    sprintf(buffer, "/midi/note/%i", index+1);
    lo_send(transmit_socket_2, buffer, "ifi", index,0.0,State);
    CPhidgetInterfaceKit_setOutputState(IFK, index, !State);
  }	else {
    sprintf(buffer, "/midi/note/%i", index+7);
  	lo_send(transmit_socket, buffer, "ifi", index,0.0,!State);
    CPhidgetInterfaceKit_setOutputState(IFK, index, State);
  }
	return 0;
}

//callback that will run if an output changes.
//Index - Index of the output that generated the event, State - boolean (0 or 1) representing the output state (on or off)
int CCONV OutputChangeHandler(CPhidgetInterfaceKitHandle IFK, void *usrptr, int Index, int State)
{
	printf("Digital Output: %d > State: %d\n", Index, State);
	return 0;
}



//Display the properties of the attached phidget to the screen.  We will be displaying the name, serial number and version of the attached device.
//Will also display the number of inputs, outputs, and analog inputs on the interface kit as well as the state of the ratiometric flag
//and the current analog sensor sensitivity.
int display_properties(CPhidgetInterfaceKitHandle phid)
{
	int serialNo, version, numInputs, numOutputs, numSensors, triggerVal, ratiometric, i;
	const char* ptr;

	CPhidget_getDeviceType((CPhidgetHandle)phid, &ptr);
	CPhidget_getSerialNumber((CPhidgetHandle)phid, &serialNo);
	CPhidget_getDeviceVersion((CPhidgetHandle)phid, &version);

	CPhidgetInterfaceKit_getInputCount(phid, &numInputs);
	CPhidgetInterfaceKit_getOutputCount(phid, &numOutputs);
	CPhidgetInterfaceKit_getSensorCount(phid, &numSensors);
  ratiometric = PFALSE;
  CPhidgetInterfaceKit_setRatiometric(phid, PFALSE);
	CPhidgetInterfaceKit_getRatiometric(phid, &ratiometric);

	printf("%s\n", ptr);
	printf("Serial Number: %10d\nVersion: %8d\n", serialNo, version);
	printf("# Digital Inputs: %d\n# Digital Outputs: %d\n", numInputs, numOutputs);
	printf("# Sensors: %d\n", numSensors);
	printf("Ratiometric: %d\n", ratiometric);

	for(i = 0; i < numSensors; i++)
	{
		CPhidgetInterfaceKit_setSensorChangeTrigger (phid, i, 2);
		CPhidgetInterfaceKit_getSensorChangeTrigger (phid, i, &triggerVal);
    

		printf("Sensor#: %d > Sensitivity Trigger: %d\n", i, triggerVal);
    //CPhidgetInterfaceKit_getDataRate (phid, i, &triggerVal);
  	//printf("Sensor#: %d > DataRate: %d\n", i, triggerVal);
	}


	return 0;
}


int interfacekit_simple()
{
	int result, numSensors, i;
	const char *err;

	//Declare an InterfaceKit handle
	CPhidgetInterfaceKitHandle ifKit = 0;
	CPhidgetInterfaceKit_create(&ifKit);
	//Set the handlers to be run when the device is plugged in or opened from software, unplugged or closed from software, or generates an error.
	//CPhidget_set_OnAttach_Handler((CPhidgetHandle)ifKit, AttachHandler, NULL);
	//CPhidget_set_OnDetach_Handler((CPhidgetHandle)ifKit, DetachHandler, NULL);
	//CPhidget_set_OnError_Handler((CPhidgetHandle)ifKit, ErrorHandler, NULL);
	//Registers a callback that will run if an input changes.
	//Requires the handle for the Phidget, the function that will be called, and an arbitrary pointer that will be supplied to the callback function (may be NULL).
	CPhidgetInterfaceKit_set_OnInputChange_Handler (ifKit, InputChangeHandler, NULL);
	CPhidgetInterfaceKit_set_OnSensorChange_Handler (ifKit, SensorChangeHandler, NULL);
  CPhidget_set_OnAttach_Handler((CPhidgetHandle)ifKit, AttachHandler, NULL);
	//CPhidgetInterfaceKit_set_OnOutputChange_Handler (ifKit, OutputChangeHandler, NULL);
	CPhidget_open((CPhidgetHandle)ifKit, 316966);

	CPhidgetInterfaceKitHandle ifKitLeft = 0;
	CPhidgetInterfaceKit_create(&ifKitLeft);
	CPhidgetInterfaceKit_set_OnInputChange_Handler (ifKitLeft, InputChangeHandler, NULL);
	CPhidgetInterfaceKit_set_OnSensorChange_Handler (ifKitLeft, SensorChangeHandler, NULL);
  CPhidget_set_OnAttach_Handler((CPhidgetHandle)ifKitLeft, AttachHandler, NULL);
  //CPhidgetInterfaceKit_set_OnOutputChange_Handler (ifKitLeft, OutputChangeHandler, NULL);
	CPhidget_open((CPhidgetHandle)ifKitLeft, 319130);
	

	//get the program to wait for an interface kit device to be attached
	printf("Waiting for interface kit to be attached....");
	if((result = CPhidget_waitForAttachment((CPhidgetHandle)ifKit, 10000)))
	{
		CPhidget_getErrorDescription(result, &err);
		printf("Problem waiting for attachment: %s\n", err);
		return 0;
	}

	//Display the properties of the attached interface kit device

	//get the number of sensors available
	CPhidgetInterfaceKit_getSensorCount(ifKit, &numSensors);

	//Change the sensitivity trigger of the sensors
	for(i = 0; i < numSensors; i++)
	{
	//	CPhidgetInterfaceKit_setSensorChangeTrigger(ifKit, i, 4);  //we'll just use 10 for fun
  //  CPhidgetInterfaceKit_setSensorChangeTrigger(ifKitLeft, i, 4);  //we'll just use 10 for fun
	}

  //CPhidgetInterfaceKit_setRatiometric(ifKit, 0);
  //CPhidgetInterfaceKit_setRatiometric(ifKitLeft, 0);

  for (i = 0; i < 6; i++) {
    CPhidgetInterfaceKit_setOutputState(ifKit, i, 0);
  }

  for (i = 0; i < 4; i++) {
     CPhidgetInterfaceKit_setOutputState(ifKitLeft, i, 0);
  }

  display_properties(ifKit);
	display_properties(ifKitLeft);

	//keep displaying interface kit data until user input is read
	printf("Press any key to end\n");
	getchar();

	//since user input has been read, this is a signal to terminate the program so we will close the phidget and delete the object we created
	printf("Closing...\n");
	CPhidget_close((CPhidgetHandle)ifKit);
	CPhidget_delete((CPhidgetHandle)ifKit);

	//all done, exit

	printf("Press any key to end\n");
	getchar();
	
	return 0;
}

int main(int argc, char* argv[])
{
	//transmit_socket = lo_address_new(arguments.ip, arguments.port);
	transmit_socket = lo_address_new("192.168.1.77", "9000");
  transmit_socket_2 = lo_address_new("192.168.1.66", "9000");
	interfacekit_simple();
	return 0;
}

